
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
	sessionId = getSessionId();

	var ime; // pridobimo ime iz polja z ID-jem kreirajIme
	var priimek;
	var spol;
	var drzava;
	var bday;
	var visina;
	var teza;
	var tem;
	var sistol;
	var diastol;
	var pulse;
	
	var ehrId;
  switch (stPacienta) {
	  	case 0:
	  		ime = "Zdravko";
	  		priimek = "Atlet";
	  		spol = "MALE";
			drzava = "Slovenia";
			bday= "1980-10-20";
			visina = 193;
			teza = 80;
			tem = 36.7;
			sistol = 118;
			diastol = 69;
			pulse = 67;
	  		break;
	  	case 1:
	  		ime = "Marija";
	  		priimek = "Visokoletna";
	  		spol = "FEMALE";
			drzava = "United Kingdom";
			bday= "1930-10-20";
			visina = 169;
			teza = 110;
			tem = 37.7;
			sistol = 130;
			diastol = 81;
			pulse = 59;
	  		break;
	  	case 2:
	  		ime = "Denis";
	  		priimek = "Padec";
	  		spol = "MALE";
			drzava = "Slovenia";
			bday= "1980-04-04";
			visina = 171;
			teza = 73;
			tem = 37.8;
			sistol = 110;
			diastol = 70;
			pulse = 120;
	  		break;
	  	default:
	  		// code
	  }
	  $.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr", // pošiljanje zahteve
		    type: 'POST',
		    success: function (data) {
		        ehrId = data.ehrId; // ko dobimo odgovor
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            gender: spol,
		            dateOfBirth: bday,
		            partyAdditionalInfo: [{key: "country", value: drzava}, {key: "height", value: visina}, {key: "weight", value: teza}, {key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party", // da vemo, kakšna je forma, moramo pogledati dokumentacijo na ehr scape
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {  // uspešno
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo label label-success fade-in'>Uspešno kreiran EHR '" + ehrId + "'.</span>");
		                    console.log("Uspešno kreiran EHR '" + ehrId + "'.");
		                    $("#preberiEHRid").val(ehrId);
		                    
		                    
		                    var datumInUra = new Date();//"2014-11-21T11:40Z";
							var telesnaVisina = visina;
							var telesnaTeza = teza;
							var telesnaTemperatura = tem;
							var sistolicniKrvniTlak = sistol;
							var diastolicniKrvniTlak = diastol;
							var utripSrca = pulse;
							var merilec = "Sam Bog";
						
							if (!ehrId || ehrId.trim().length == 0) {
								$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
							} else {
								$.ajaxSetup({
								    headers: {"Ehr-Session": sessionId}
								});
								var podatki = { // source predloge za vital signs
									// Preview Structure: https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
								    "ctx/language": "en",
								    "ctx/territory": "SI",
								    "ctx/time": datumInUra,
								    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
								    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
								   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
								    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
								    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
								    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
								    "vital_signs/pulse:0/any_event/rate|magnitude": utripSrca
								};
								var parametriZahteve = { // klic template-a
								    "ehrId": ehrId,
								    templateId: 'Vital Signs', // template id
								    format: 'FLAT',
								    committer: merilec
								};
								$.ajax({
								    url: baseUrl + "/composition?" + $.param(parametriZahteve),
								    type: 'POST',
								    contentType: 'application/json',
								    data: JSON.stringify(podatki),
								    success: function (res) { // pošiljanje podatkov na server
								    	console.log(res.meta.href);
								    	$("#kreirajSporocilo").html("<span class='obvestilo label label-success fade-in'>Uspešno kreiran EHR '" + ehrId + "' + dodani podatki.</span>");
								    	document.getElementById('preberiObstojeciEHR').options[stPacienta+1].value = ehrId;
								    },
								    error: function(err) {
								    	$("#kreirajSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
										console.log(JSON.parse(err.responseText).userMessage);
								    }
								});
							}
		                    
		                }
		            },
		            error: function(err) {  // neuspešno
		            	$("#kreirajSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
		            	console.log(JSON.parse(err.responseText).userMessage);
		            }
		        });
		    }
		});

  return ehrId;
}


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija

function kreirajEHRzaBolnika() {
	sessionId = getSessionId();

	var ime = $("#kreirajIme").val(); // pridobimo ime iz polja z ID-jem kreirajIme
	var priimek = $("#kreirajPriimek").val();
	var spol = $("#kreirajSpol").val();
	var drzava = $("#kreirajDrzavo").val();
	var bday = $("#kreirajBday").val();
	var visina = $("#kreirajVisino").val();
	var teza = $("#kreirajTezo").val();
	var tem = $("#kreirajTem").val();
	var sistol = $("#kreirajSistol").val();
	var diastol = $("#kreirajDiastol").val();
	var pulse = $("#kreirajPulse").val();
	
	var ehrId;

	if (!ime || !priimek || !spol ||!drzava || !bday || !tem || !sistol || !diastol || !pulse || !visina || !teza || 
	   ime.trim().length == 0 || priimek.trim().length == 0 || visina.trim().length == 0 || teza.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr", // pošiljanje zahteve
		    type: 'POST',
		    success: function (data) {
		        ehrId = data.ehrId; // ko dobimo odgovor
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            gender: spol,
		            dateOfBirth: bday,
		            partyAdditionalInfo: [{key: "country", value: drzava}, {key: "height", value: visina}, {key: "weight", value: teza}, {key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party", // da vemo, kakšna je forma, moramo pogledati dokumentacijo na ehr scape
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {  // uspešno
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo label label-success fade-in'>Uspešno kreiran EHR '" + ehrId + "'.</span>");
		                    console.log("Uspešno kreiran EHR '" + ehrId + "'.");
		                    $("#preberiEHRid").val(ehrId);
		                    
		                    
		                    var datumInUra = new Date();//"2014-11-21T11:40Z";
							var telesnaVisina = visina;
							var telesnaTeza = teza;
							var telesnaTemperatura = tem;
							var sistolicniKrvniTlak = sistol;
							var diastolicniKrvniTlak = diastol;
							var utripSrca = pulse;
							var merilec = "Sam Bog";
						
							if (!ehrId || ehrId.trim().length == 0) {
								$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
							} else {
								$.ajaxSetup({
								    headers: {"Ehr-Session": sessionId}
								});
								var podatki = { // source predloge za vital signs
									// Preview Structure: https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
								    "ctx/language": "en",
								    "ctx/territory": "SI",
								    "ctx/time": datumInUra,
								    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
								    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
								   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
								    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
								    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
								    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
								    "vital_signs/pulse:0/any_event/rate|magnitude": utripSrca
								};
								var parametriZahteve = { // klic template-a
								    "ehrId": ehrId,
								    templateId: 'Vital Signs', // template id
								    format: 'FLAT',
								    committer: merilec
								};
								$.ajax({
								    url: baseUrl + "/composition?" + $.param(parametriZahteve),
								    type: 'POST',
								    contentType: 'application/json',
								    data: JSON.stringify(podatki),
								    success: function (res) { // pošiljanje podatkov na server
								    	console.log(res.meta.href);
								    	$("#kreirajSporocilo").html("<span class='obvestilo label label-success fade-in'>Uspešno kreiran EHR '" + ehrId + "' + dodani podatki.</span>");
								    	
								    },
								    error: function(err) {
								    	$("#kreirajSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
										console.log(JSON.parse(err.responseText).userMessage);
								    }
								});
							}
		                    
		                }
		            },
		            error: function(err) {  // neuspešno
		            	$("#kreirajSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
		            	console.log(JSON.parse(err.responseText).userMessage);
		            }
		        });
		    }
		});
	}
}

function preberiEHRodBolnika() {
    
    var x = document.getElementById("myDIV");
    if (x.style.display === "block") {
        x.style.display = "none";
    }
    
	sessionId = getSessionId();

	var ehrId = $("#preberiEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning fade-in'>Prosim vnesite zahtevan podatek!</span>");
	} else {
		
		var teza, visina, spol, bday, tem, sistol, diastol, puls;
		 
        $.ajax({
            url: baseUrl + "/view/" + ehrId + "/body_temperature",
            type: 'GET',
            headers: {
                "Ehr-Session": sessionId
            },
            success: function (data) {
                tem = data[0].temperature;
                console.log(tem);
                
                generateTemperaturaChart(tem);
                $("#temperaturaChart").load(" #temperaturaChart");
                $("#temperatura").css("visibility","visible");
            }
        });
        
        $.ajax({
            url: baseUrl + "/view/" + ehrId + "/blood_pressure",
            type: 'GET',
            headers: {
                "Ehr-Session": sessionId
            },
            success: function (data) {
            	
                sistol = data[0].systolic;
                diastol = data[0].diastolic;
                generateBloodPressureGraph(sistol, diastol);
                $("#bloodPressureChartDiastolic").load(" #bloodPressureChartDiastolic");
                $("#bloodPressureChartSystolic").load(" #bloodPressureChartSystolic");
                console.log(sistol);
            }
        });
        
        $.ajax({
            url: baseUrl + "/view/" + ehrId + "/pulse",
            type: 'GET',
            headers: {
                "Ehr-Session": sessionId
            },
            success: function (data) {
                puls = data[0].pulse;
                generatePulsChart(puls);
                $("#pulsChart").load(" #pulsChart");
                console.log(puls);
            }
        });
		
		
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {

				var party = data.party;
				spol = party.gender;
				bday = party.dateOfBirth;
				visina = party.additionalInfo.height;
				teza = party.additionalInfo.weight;
				
				console.log(party);
				
				
			},
			error: function(err) {
				$("#preberiSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
				console.log(JSON.parse(err.responseText).userMessage);
			}
		});
	}setTimeout(function(){
    	$('html,body').animate({scrollTop: document.body.scrollHeight},"slow");
	}, 1000);
	
}

function generateBloodPressureGraph(sistol, diastol){
	$("#bloodPressureChartSystolic").remove();
	$("#bloodPressureChartDiastolic").remove();
	$("#pritiskSis").append('<canvas id="bloodPressureChartSystolic"  width="100px" height="100px"></canvas>');
	$("#pritiskDis").append('<canvas id="bloodPressureChartDiastolic"  width="100px" height="100px"></canvas>');
    var color;
    if(sistol > 140){ color = 'red'; console.log("too much");}
    else if(sistol >120) color='orange';// draw negative values in red
    else color = 'green';
    var ctx = document.getElementById("bloodPressureChartSystolic").getContext('2d');
    var bloodPressureChartSystolic = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: [ "Oseba"],
            datasets: [{
                label: 'Sistol',
                data: [sistol],
                backgroundColor: [color],
                borderColor: [color],
                borderWidth: 1
                    
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
    
    if(diastol > 90){ color = 'red'; console.log("too much");}
    else if(diastol >80) color='orange';// draw negative values in red
    else color = 'green';
    var ctx2 = document.getElementById("bloodPressureChartDiastolic").getContext('2d');
    var bloodPressureChartDiastolic = new Chart(ctx2, {
        type: 'bar',
        data: {
            labels: [ "Oseba"],
            datasets: [{
                label: 'Diastol',
                data: [diastol],
                backgroundColor: [color],
                borderColor: [color],
                borderWidth: 1
                    
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
}

function generateTemperaturaChart(tem){
	$("#temperaturaChart").remove();
	$("#temperatura").append('<canvas id="temperaturaChart" width="100px" height="100px"></canvas>');
    var color;
    if(tem < 36.5||tem>37){ color = 'red'; console.log("too much")}  // draw negative values in red
    else color = 'green';
    var ctx = document.getElementById("temperaturaChart").getContext('2d');
    var temperaturaChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: [ "Oseba"],
            datasets: [{
                label: 'Temperatura',
                data: [tem],
                backgroundColor: [color],
                borderColor: [color],
                borderWidth: 1
                    
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
}

function generatePulsChart(puls){
	$("#pulsChart").remove();
	$("#puls").append('<canvas id="pulsChart" width="100px" height="100px"></canvas>');
    var color;
    if(puls > 100||puls<60){ color = 'red'; console.log("too much");}
    else if(puls >90) color='orange';// draw negative values in red
    else color = 'green';
    var ctx = document.getElementById("pulsChart").getContext('2d');
    var pulsChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: [ "Oseba"],
            datasets: [{
                label: 'Pulz',
                data: [puls],
                backgroundColor: [color],
                borderColor: [color],
                borderWidth: 1
                    
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
}

$(document).ready(function() {
    $("#temperatura").css("visibility","hidden");
    
   $.get("https://api.ipdata.co", function (odgovor) {
        		console.log(odgovor.city);
        		$.getJSON("https://api.openweathermap.org/data/2.5/weather",
				{
					APPID:"72cd0057d6d6224c69e423bc0e8f1217",
					q:odgovor.city
				},
				function(response){  //'response' is actually a variable, we could name it what ever we want. It just stores what the api sends
					console.log(response)
					$(".country").html(response.sys.country)
					$(".temp").html(Math.round(response.main.temp-273) + " stopinj Celzija");
					$(".city-name").html(response.name);
					  $(".weather").html(response.weather[0].description)
				});
        	});

  /**
   * Napolni testne vrednosti (ime, priimek in datum rojstva) pri kreiranju
   * EHR zapisa za novega bolnika, ko uporabnik izbere vrednost iz
   * padajočega menuja (npr. Pujsa Pepa).
   */
  $('#preberiPredlogoBolnika').change(function() {
    $("#kreirajSporocilo").html("");
    var podatki = $(this).val().split(";");
    $("#kreirajIme").val(podatki[0]);
    $("#kreirajPriimek").val(podatki[1]);
    $("#kreirajSpol").val(podatki[2]);
    $("#kreirajDrzavo").val(podatki[3]);
    $("#kreirajBday").val(podatki[4]);
    $("#kreirajVisino").val(podatki[5]);
    $("#kreirajTezo").val(podatki[6]);
    $("#kreirajTem").val(podatki[7]);
    $("#kreirajSistol").val(podatki[8]);
    $("#kreirajDiastol").val(podatki[9]);
    $("#kreirajPulse").val(podatki[10]);
  });

  /**
   * Napolni testni EHR ID pri prebiranju EHR zapisa obstoječega bolnika,
   * ko uporabnik izbere vrednost iz padajočega menuja
   * (npr. Dejan Lavbič, Pujsa Pepa, Ata Smrk)
   */
	$('#preberiObstojeciEHR').change(function() {
		$("#preberiSporocilo").html("");
		$("#preberiEHRid").val($(this).val());
	});
	
	
	$("#generateDataButton").on('click', function(){
		for(var i = 0; i < 3; i++){
			generirajPodatke(i);
		}
		alert("Posodobljeni so bili ehr id-ji in dodani podatki za tri osebe");
	});
    
});